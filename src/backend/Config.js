const webClientId =
  '989283941157-to32mv07uo1tp9b2kn4ir0ftp0uquip4.apps.googleusercontent.com';
const BASE_URL = 'http://139.59.76.223/lol/webservices';
const XApiKey = 'c3a3cf7c211b7c07b2495d8aef9761fc';
export const GOOGLE_MAPS_APIKEY = 'AIzaSyD7BIoSvmyufubmdVEdlb2sTr4waQUexHQ';
// export const GOOGLE_MAPS_APIKEY = 'AIzaSyCCJVEWTfTM7V9n5Qn2csaCyi7dvZopdKU'
export const PATH_URL = {
  SignIn: '/signIn',
  SignUp: '/signUp',
  RegisterOtp: '/register_otp',
  LoginOtp: '/login_otp',
  GetProfile: '/getprofile',
  EditProfile: '/edit_user_profile',
};

export const GoogleSigninJson = {
  webClientId: webClientId,
  offlineAccess: true,
  hostedDomain: '',
  forceConsentPrompt: true,
};

export const ApiSauceJson = {
  baseURL: BASE_URL,
  headers: {
    'X-API-KEY': XApiKey,
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
};

export const latitudeDelta = 0.0922;
