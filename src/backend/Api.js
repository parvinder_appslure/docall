import AsyncStorage from '@react-native-community/async-storage';
import {PATH_URL} from './Config';
import ApiSauce from './ApiSauce';
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoder';
import {Dimensions} from 'react-native';
import {latitudeDelta} from '../backend/Config';
const request = (path, json) => {
  return new Promise((resolve, reject) => {
    ApiSauce.post(path, json).then((response) => {
      if (response.ok) {
        resolve(response.data);
      } else {
        console.log(response.err);
        reject(response.err);
      }
    });
  });
};

export const SignUpApi = (json) => request(PATH_URL.SignUp, json);
export const SignInApi = (json) => request(PATH_URL.SignIn, json);
export const RegisterOtpApi = (json) => request(PATH_URL.RegisterOtp, json);
export const LoginOtpApi = (json) => request(PATH_URL.LoginOtp, json);
export const GetProfileApi = (json) => request(PATH_URL.GetProfile, json);

export const AsyncStorageSetUser = (user) =>
  AsyncStorage.setItem('user', JSON.stringify(user));
export const AsyncStorageGetUser = () => AsyncStorage.getItem('user');
export const AsyncStorageClear = () => AsyncStorage.clear();

export const GeolocationInfo = () => {
  return new Promise((resolve, reject) => {
    Geolocation.getCurrentPosition((info) => {
      resolve(info);
    });
  });
};

export const GeocoderLocation = (lat, lng) => {
  return new Promise((resovle, reject) => {
    Geocoder.geocodePosition({lat: lat, lng: lng}).then((res) => resovle(res));
  });
};

export const AspectRatio = () =>
  Dimensions.get('window').width / Dimensions.get('window').height;
export const Height = Dimensions.get('window').height;
export const Width = Dimensions.get('window').width;
export const LongitudeDelta = () =>
  (latitudeDelta * Dimensions.get('window').width) /
  Dimensions.get('window').height;
export const LatitudeDelta = latitudeDelta;

export const formatAmount = (amount) =>
  `\u20B9 ${parseInt(amount)
    .toFixed(0)
    .replace(/(\d)(?=(\d\d)+\d$)/g, '$1,')}`;

export const formatNumber = (str) =>
  str.replace(/,/g, '').replace('\u20B9 ', '');

export const textInPrice = (price) => `\u20B9 ${price}`;

export const timeFormate_mmss = (time) => {
  let mm = Math.floor(time / 60);
  let ss = time % 60;
  mm = mm < 10 ? `0${mm}` : mm;
  ss = ss < 10 ? `0${ss}` : ss;
  return `${mm}:${ss}`;
};
